import React from 'react';
import ResponsiveContainer from "recharts/lib/component/ResponsiveContainer";
import PieChart from "recharts/lib/chart/PieChart";
import PieRecharts from "recharts/lib/polar/Pie";
import Cell from "recharts/lib/component/Cell";
import TooltipRecharts from "recharts/lib/component/Tooltip";
import TooltipSingle from '../Tooltip/TooltipSingle'
import LoadingAnimation from '../LoadingAnimation/LoadingAnimation';

const renderCustomizedLabel = ({
  cx,
  cy,
  midAngle,
  innerRadius,
  outerRadius,
  value
}) => {
  const RADIAN = Math.PI / 180;
  const radius = 25 + innerRadius + (outerRadius - innerRadius);
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text
      x={x}
      y={y}
      fontSize='12px'
      fill='#767676'
      textAnchor={x > cx ? "start" : "end"}
      dominantBaseline="central"
    >
      {`${(Math.round(value * 10) / 10).toLocaleString('nl-NL')}%`}
    </text>
  );
};

const Pie = ({ data, height = 360, colors, isLoading, dataLabel }) => {
 return (
    <ResponsiveContainer width='100%' height={height}>
      {!isLoading ?
      <PieChart>
        <PieRecharts
          animationDuration={550}
          data={data}
          dataKey="value"
          innerRadius='35%'
          outerRadius='60%'
          fill="#82ca9d"
          label={renderCustomizedLabel}
        >
          {
            data.map((entry, index) => (
              <Cell key={`cell-${index}`} fill={colors ? colors[index] : '#004699'} />
            ))
          }
        </PieRecharts>
        <TooltipRecharts content={<TooltipSingle tooltipLabel={dataLabel} />} />
      </PieChart> :
      <LoadingAnimation />}
    </ResponsiveContainer>
  )
}

export default Pie;