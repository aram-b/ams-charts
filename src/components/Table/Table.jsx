import React from 'react'
import { themeColor, breakpoint } from '@datapunt/asc-ui'
import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-end;
`

const TitleBar = styled.div`
  width: calc(50% - 8px);
  margin-right: 8px;

  @media screen and ${breakpoint('max-width', 'laptop')} {
    display: none;
  }
`
const TitleWrapper = styled.div`
  display: inline-block;
  width: calc(100% / 3 - 1px);
  text-align: center;
`

const Title = styled.span`
  display: inline-block;
  margin-right: 8px;
  padding: 12px;
  text-align: center;
`

const Container = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  margin-bottom: 12px;
  padding: 8px;
  background-color: ${themeColor('tint', 'level2')};

  &:last-child {
    margin-bottom: 0;
  }
`

const Name = styled.span`
  width: 50%;
  align-self: flex-end;
  padding: 12px;

  @media screen and ${breakpoint('max-width', 'laptop')} {
    width: 100%;
  }
`

const CardContainer = styled.div`
  width: calc(50% / 3 - 1px);
  display: flex;
  flex-direction: column;
  position: relative;

  @media screen and ${breakpoint('max-width', 'laptop')} {
    width: calc(100% / 3 - 1px);
  }
`

const CardTitle = styled.span`
  margin-right: 8px;
  padding-left: 12px;
  padding-right: 12px;
  padding-bottom: 12px;
  text-align: center;
  display: none;

  @media screen and ${breakpoint('max-width', 'laptop')} {
    display: inline;
  }
`

const CardValue = styled.span`
  position: relative;
  margin-right: 8px;
  padding: 12px;
  text-align: center;
  font-weight: bold;
  color: ${({ value }) => 
    value >= 5.5 && value < 7.75 ? 'black' : 'white'
  };
  background-color: ${({ value }) => 
    value >= 1 && value < 3.25 ? themeColor('secondary', 'main') :
    value >= 3.25 && value < 5.5 ? themeColor('supplement', 'orange') :
    value >= 5.5 && value < 7.75 ? themeColor('supplement', 'lightgreen') :
    value >= 7.75 && value <= 10 ? themeColor('supplement', 'darkgreen') :
    themeColor('primary', 'main')
  };
  &:after {
    position: absolute;
    font-size: 12px;
    top: 0;
    right: 4px;
    content: '${({ value }) =>
      value >= 1 && value < 3.25 ? '- -' :
      value >= 3.25 && value < 5.5 ? '-' :
      value >= 5.5 && value < 7.75 ? '+' :
      value >= 7.75 && value <= 10 ? '++' :
      ''
    }';
  }
  &:before {
    content: "";
    width: 0px;
    height: 0px;
    position: absolute;
    right: 0px;
    bottom: 0px;
    border-width: 4px;
    border-color: transparent ${themeColor('tint', 'level2')} ${themeColor('tint', 'level2')} transparent;
    border-style: solid;
  }
`

const Table = ({ data }) => {
  return (
    <Wrapper>
      <TitleBar>
        {data.title.map(entry => (
          <TitleWrapper key={entry}>
            <Title>{entry}</Title>
          </TitleWrapper>
        ))}
      </TitleBar>
      {data ? data.data.map((entry, idx) => (
        <Container key={idx}>
          <Name>{entry.name}</Name>
          {entry.data.map((item, idz) => (
            <CardContainer key={idz}>
              <CardTitle>{data.title[idz]}</CardTitle>
              <CardValue value={item}>{item ? (Math.round(item * 10) / 10).toLocaleString('nl-NL') : '-'}</CardValue>
            </CardContainer>
          ))}
        </Container>
      )) : null}
    </Wrapper>
  )
}

export default Table