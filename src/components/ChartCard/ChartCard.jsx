import React from 'react';
import styled from 'styled-components';

const StyledCard = styled.div`
  width: 100%;
  overflow: hidden;
`

const ChartCard = ({ children, title }) => (
  <StyledCard>
    {title}
    {children}
  </StyledCard>
)

export default ChartCard;