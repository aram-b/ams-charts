import React, { useState, useEffect, useRef } from 'react';
import ResponsiveContainer from "recharts/lib/component/ResponsiveContainer";
import LineChart from "recharts/lib/chart/LineChart";
import CartesianGrid from "recharts/lib/cartesian/CartesianGrid";
import LineRecharts from "recharts/lib/cartesian/Line";
import XAxis from "recharts/lib/cartesian/XAxis";
import ReferenceLine from "recharts/lib/cartesian/ReferenceLine";
import YAxis from "recharts/lib/cartesian/YAxis";
import TooltipRecharts from "recharts/lib/component/Tooltip";
import LegendRecharts from "recharts/lib/component/Legend";
import Label from "recharts/lib/component/Label";
import Tooltip from '../Tooltip/TooltipMulti'
import Legend from '../Legend/Legend'
import LoadingAnimation from '../LoadingAnimation/LoadingAnimation'
import { min as d3min, max as d3max } from 'd3-array'
import { getNiceTickValues } from 'recharts-scale'

const XAxisTicksAms = ({ x, y, payload }) => (
  <g transform={`translate(${x},${y})`}>
    <text
    dy={20}
    textAnchor='middle'
    fill={'#767676'}
    fontSize='14px'>
      {payload.value}
    </text>
  </g>
)

const CustomizedLabel = ({ viewBox, value }) => {
  const labelRef = useRef(null);

  useEffect(() => {
    const text = labelRef.current.getElementsByTagName('text')[0]
    const rect = labelRef.current.getElementsByTagName('rect')[0]
    const bounds = text.getBoundingClientRect()

    rect.setAttribute('width', bounds.width + 8)
    rect.setAttribute('height', bounds.height)
    rect.setAttribute('transform', `translate(0 -${bounds.height / 2})`)
  })

  return (
    <g ref={labelRef}>
      <rect x={viewBox.width - 4} y={viewBox.y} fill='white' />
      <text x={viewBox.width} y={viewBox.y} dy={5} fontSize='14px' fill='#767676'>
        <tspan>{value}</tspan>
      </text>
    </g>
  )
}

const getBufferedTicks = (data) => {
  const values = [].concat.apply([], data.map(entry => entry.data)).map(item => item.value)
  const min = d3min(values)
  const max = d3max(values)
  const buffer = 0.25
  const minVal = min >= 0 ? 0 : min + (min*buffer)
  const maxVal = max <= 0 ? 0 : max + (max*buffer)

  return getNiceTickValues([minVal, maxVal], 5, false)
}

const Line = ({ data, height = 400, colors, dataLabel, yAxisLabel = '%', xAxisLabel, isLoading }) => {
  const [legendFilter, setLegendFilter] = useState([])

  // get unique categories in data
  const uniqueCats = !isLoading && [...new Set([].concat.apply([], data.map(entry => entry.data)).map(item => item.name))];

  // get filtered data for nice ticks
  const filteredData = !isLoading && data.map(entry => (
    {
      name: entry.name, 
      data: entry.data.filter(item => !legendFilter.includes(item.name))
    }
  ))

  return (
    <ResponsiveContainer width='100%' height={height} minWidth={0}>
      {!isLoading ?
      <LineChart data={data} margin={{ right: 55, top: 33, bottom: 20 }}>
        {uniqueCats.length > 1 ?
        <LegendRecharts
          align='left'
          verticalAlign='top'
          wrapperStyle={{ top: 0 }}
          content={
            <Legend
              legendFilter={legendFilter}
              setLegendFilter={setLegendFilter}
              iconType='plainline'
            />
          }
        /> : null}
        <CartesianGrid vertical={false} stroke='#e6e6e6' />
        <XAxis
          dataKey='name'
          tickSize={0}
          tick={<XAxisTicksAms />}
          axisLine={false}
        >
          {xAxisLabel ?
          <Label
            position='insideBottomRight'
            offset={0}
            dx={4}
            dy={16}
            style={{ fontSize: '14px', fill: '#767676' }}
          >
            {xAxisLabel}
          </Label> : null}
        </XAxis>
        <ReferenceLine y={0} stroke="black" strokeWidth='2px' />
        <TooltipRecharts
          position={{ y: 200 }}
          offset={10}
          animationDuration={250}
          content={<Tooltip tooltipLabel={dataLabel} tooltipSort />}
          cursor={{ fill: '#e6e6e6', strokeWidth: '2px', strokeDasharray: '2,2' }}
        />
        {uniqueCats.map((entry, idx) => {
          return (
            <LineRecharts
              key={legendFilter.indexOf(entry) === -1 ? `${entry}_show` : `${entry}_hide`}
              name={entry}
              dataKey={x => legendFilter.indexOf(entry) === -1 ? x.data.find(item => item.name === entry).value : null}
              stroke={
                colors && legendFilter.indexOf(entry) === -1 ? colors[idx] :
                !colors ? '#004699' :
                '#e6e6e6'
              }
              strokeWidth='2px'
              dot={false}
              animationDuration={350}
              className='test'
            />
          )
        })}
        <YAxis
          width={55}
          axisLine={false}
          tickSize={0}
          tick={{ fontSize: '14px', fill: '#767676', transform: 'translate(-4, 0)' }}
          label={{ value: yAxisLabel, content: <CustomizedLabel /> }}
          tickFormatter={tick => tick.toLocaleString('nl-NL')}
          ticks={getBufferedTicks(filteredData)}
        />
      </LineChart> :
      <LoadingAnimation />}
    </ResponsiveContainer>
  )
}

export default Line;