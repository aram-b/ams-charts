import styled, { keyframes } from 'styled-components';
import { themeColor } from '@datapunt/asc-ui';

const LoadingKeyframes = ({ theme }) => keyframes`
	0% { background-color: ${themeColor('tint', 'level4')({ theme })} }
	50% { background-color: ${themeColor('tint', 'level2')({ theme })} }
	100% { background-color: ${themeColor('tint', 'level4')({ theme })} }
`

const LoadingAnimation = styled.div`
	height: 100%;
	animation-name: ${LoadingKeyframes};
	animation-duration: 2s;
	animation-timing-function: ease-in-out;
	animation-iteration-count: infinite;
`

export default LoadingAnimation;