import React, { useState } from 'react';
import ResponsiveContainer from "recharts/lib/component/ResponsiveContainer";
import BarChart from "recharts/lib/chart/BarChart";
import Bar from "recharts/lib/cartesian/Bar";
import XAxis from "recharts/lib/cartesian/XAxis";
import YAxis from "recharts/lib/cartesian/YAxis";
import LabelList from "recharts/lib/component/LabelList";
import Tooltip from "recharts/lib/component/Tooltip";
import LegendRecharts from "recharts/lib/component/Legend";
import TooltipAms from '../Tooltip/TooltipMulti';
import LoadingAnimation from '../LoadingAnimation/LoadingAnimation';
import Legend from '../Legend/Legend';

const CustomLabel = ({ x, y, width, value }) => {
  if (x) {
    return (
      <text
        x={(width/2) + x}
        y={y}
        dy={-4}
        fontSize='12px'
        fill='#767676'
        textAnchor='middle'
      >
        {(Math.round(value * 10) / 10).toLocaleString('nl-NL')}
      </text>
    )
  }
  return null
}

const ClusteredBar = ({ data, height = 400, colors, isLoading, dataLabel, zeroText = ''}) => {
  const [legendFilter, setLegendFilter] = useState([]);

  return (
    <ResponsiveContainer width='100%' height={height} minWidth={0}>
      {!isLoading ?
      <BarChart data={data} margin={{ top: 28 }}>
        <LegendRecharts
        align='left'
        verticalAlign='top'
        wrapperStyle={{ top: 0 }}
        content={<Legend legendFilter={legendFilter} setLegendFilter={setLegendFilter} iconType='rect' />} />
        <YAxis type='number' hide />
        <Tooltip
          cursor={{ fill: '#F5F5F5' }}
          position={{ y: 200 }}
          offset={84}
          animationDuration={250}
          content={<TooltipAms tooltipLabel={dataLabel} />}
        />
        {data[1].data.map((entry, idx) => 
          entry.name ? <Bar
            name={entry.name}
            key={legendFilter.indexOf(entry.name) === -1 ? `${idx}_show` : `${idx}_hide`}
            dataKey={ x => x.data[idx] && legendFilter.indexOf(x.data[idx].name) === -1 ? x.data[idx].value : null }
            fill={
              legendFilter.indexOf(entry.name) === -1 && colors ? colors[idx] :
              legendFilter.indexOf(entry.name) === -1 ? '#004699' :
              '#e6e6e6'
            }
            maxBarSize={36}
            animationDuration={350}
          >
            <LabelList
              dataKey={ x => x.data[idx] && legendFilter.indexOf(x.data[idx].name) === -1 ? x.data[idx].value : null }
              content={<CustomLabel zeroText={zeroText} />}
            />
          </Bar> : null
        )}
        <XAxis
          width={72}
          type='category'
          dataKey='name'
          tickSize={0}
          tick={{ fontSize: '14px', fill: '#767676', transform: 'translate(0, 8)' }}
          stroke='black'
          strokeWidth='2px'
        />
      </BarChart> :
      <LoadingAnimation />}
    </ResponsiveContainer>
  )
}

export default ClusteredBar;