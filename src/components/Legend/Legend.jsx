import React from 'react';
import styled from 'styled-components';
import { themeColor, breakpoint } from '@datapunt/asc-ui';

const StyledLegendButton = styled.button`
  border: none;
  background-color: transparent;
  cursor: pointer;
  text-align: left;
  position: relative;
  display: inline-block;
  padding-left: 24px;
  width: 50%;

  &:focus {
    outline: none;
    background-color: ${themeColor('support', 'focus')};
  }
  @media screen and ${breakpoint('max-width', 'laptop')} {
    width: 100%;
  }
`

const StyledLegendSpan = styled.span`
  color: ${props => props.filtered ? '#e6e6e6' : '#767676' };
  font-size: 14px;
  vertical-align: top;
`

const Icon = ({ iconSize, iconType, color}) => (
  <svg width={iconSize} height={iconSize} pointerEvents='none' style={{ position: 'absolute', left: 6 }}>
    {iconType === 'plainline' ? <line strokeWidth='2' fill='none' stroke={color} x1='0' x2={iconSize} y1={iconSize/2} y2={iconSize/2} /> :
    <rect stroke='none' fill={color} width={iconSize} height={iconSize - 4} y={3} />}
  </svg>
) 

const Legend = ({ payload, iconSize, legendFilter, setLegendFilter, iconType }) => {

  const handleClick = (value) => {
    legendFilter.indexOf(value) === -1 ?
    setLegendFilter([...legendFilter, value]) :
    setLegendFilter(legendFilter.filter(item => item !== value))
  }

  if (payload) {
    return (
      <>
        {payload.map((item, idx) => 
          <StyledLegendButton onClick={() => handleClick(item.value)} key={idx}>
            <Icon iconSize={iconSize} iconType={iconType} color={item.color} />
            <StyledLegendSpan
              filtered={legendFilter.indexOf(item.value) === -1 ? false : true}
            >
              
              {item.value}
            </StyledLegendSpan>
          </StyledLegendButton>
        )}
      </>
    )
  } else return null;
}

export default Legend;