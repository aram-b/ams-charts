import React, { useState, useEffect, useRef } from 'react';
import ResponsiveContainer from "recharts/lib/component/ResponsiveContainer";
import BarChart from "recharts/lib/chart/BarChart";
import Bar from "recharts/lib/cartesian/Bar";
import XAxis from "recharts/lib/cartesian/XAxis";
import YAxis from "recharts/lib/cartesian/YAxis";
import TooltipRecharts from "recharts/lib/component/Tooltip";
import LegendRecharts from "recharts/lib/component/Legend";
import CartesianGrid from "recharts/lib/cartesian/CartesianGrid";
import Tooltip from '../Tooltip/TooltipMulti';
import LoadingAnimation from '../LoadingAnimation/LoadingAnimation';
import Legend from '../Legend/Legend';

const CustomizedLabel = ({ viewBox, value }) => {
  const labelRef = useRef(null);

  useEffect(() => {
    const text = labelRef.current.getElementsByTagName('text')[0]
    const rect = labelRef.current.getElementsByTagName('rect')[0]
    const bounds = text.getBoundingClientRect()

    rect.setAttribute('width', bounds.width + 8)
    rect.setAttribute('height', bounds.height)
    rect.setAttribute('transform', `translate(0 -${bounds.height / 2})`)
  })

  return (
    <g ref={labelRef}>
      <rect x={viewBox.width - 4} y={viewBox.y} fill='white' />
      <text x={viewBox.width} y={viewBox.y} dy={5} fontSize='14px' fill='#767676'>
        <tspan>{value}</tspan>
      </text>
    </g>
  )
}

const StackedBar = ({ data, height = 400, colors, isLoading, dataLabel, yAxisLabel }) => {

  const [legendFilter, setLegendFilter] = useState([]);

  return (
    <ResponsiveContainer width='100%' height={height} minWidth={0}>
      {!isLoading ?
      <BarChart data={data} margin={{ top: 28 }}>
        <LegendRecharts
          align='left'
          verticalAlign='top'
          wrapperStyle={{ top: 0 }}
          content={<Legend legendFilter={legendFilter} setLegendFilter={setLegendFilter} iconType='rect' />}
        />
        <CartesianGrid vertical={false} stroke='#e6e6e6' />
        <TooltipRecharts
          cursor={{ fill: '#F5F5F5' }}
          position={{ y: 200 }}
          offset={72}
          animationDuration={250}
          content={<Tooltip tooltipLabel={dataLabel} />}
        />
        {data[1].data.map((entry, idx) => 
          entry.name ? 
          <Bar
            stackId="a"
            name={entry.name}
            key={legendFilter.indexOf(entry.name) === -1 ? `${idx}_show` : `${idx}_hide`}
            dataKey={ x => x.data[idx] && legendFilter.indexOf(x.data[idx].name) === -1 ? x.data[idx].value : null }
            fill={
              legendFilter.indexOf(entry.name) === -1 && colors ? colors[idx] :
              legendFilter.indexOf(entry.name) === -1 ? '#004699' :
              '#e6e6e6'
            }
            maxBarSize={36}
            animationDuration={350}
          /> : null
        )}
        <XAxis
          width={72}
          type='category'
          dataKey='name'
          tickSize={0}
          tick={{ fontSize: '14px', fill: '#767676', transform: 'translate(0, 8)' }}
          stroke='black'
          strokeWidth='2px'
        />
        <YAxis
          ticks={[0,25,50,75,100]}
          domain={[0,100]}
          width={55}
          axisLine={false}
          tickSize={0}
          tick={{ fontSize: '14px', fill: '#767676', transform: 'translate(-4, 0)' }}
          label={{ value: yAxisLabel, content: <CustomizedLabel /> }}
          allowDecimals={false}
          tickFormatter={tick => tick.toLocaleString('nl-NL')}
        />
      </BarChart> :
      <LoadingAnimation />}
    </ResponsiveContainer>
  )
}

export default StackedBar;