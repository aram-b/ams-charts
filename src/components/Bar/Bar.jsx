import React from 'react';
import ResponsiveContainer from "recharts/lib/component/ResponsiveContainer";
import BarChart from "recharts/lib/chart/BarChart";
import BarRecharts from "recharts/lib/cartesian/Bar";
import XAxis from "recharts/lib/cartesian/XAxis";
import YAxis from "recharts/lib/cartesian/YAxis";
import LabelList from "recharts/lib/component/LabelList";
import Tooltip from "recharts/lib/component/Tooltip";
import TooltipSingle from '../Tooltip/TooltipSingle';
import LoadingAnimation from '../LoadingAnimation/LoadingAnimation';

const Bar = ({ data, height = 500, color, isLoading, zeroText = '', dataLabel }) => {
  return (
    <ResponsiveContainer width='100%' height={height}>
      {!isLoading ?
      <BarChart data={data} margin={{ top: 0, right: 32, bottom: 5 }} layout='vertical' barCategoryGap={14}>
        <XAxis type='number' hide />
        <YAxis
          width={132}
          type='category'
          dataKey='name'
          axisLine={false}
          tickSize={0}
          tick={{ fontSize: '14px', fill: '#767676', transform: 'translate(-4, 0)' }}
        />
        <Tooltip
          cursor={{ fill: '#F5F5F5' }}
          position={{ x: 0 }}
          wrapperStyle={{ left: 'auto', right: 32 }}
          offset={28}
          animationDuration={250}
          content={<TooltipSingle tooltipLabel={dataLabel} />}
        />
          <BarRecharts
            dataKey='value'
            fill={color}
            maxBarSize={36}
            animationDuration={350}
          >
            <LabelList
              dataKey='value'
              position='right'
              offset={4}
              style={{ fontSize: '12px', fill: '#767676' }}
              formatter={
                value => value === 0 ? `${value} ${zeroText}` : (Math.round(value * 10) / 10).toLocaleString('nl-NL')
              }
            />
          </BarRecharts>
      </BarChart> :
      <LoadingAnimation />}
    </ResponsiveContainer>
  )
}

export default Bar;