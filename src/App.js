import React, { useState, useEffect } from 'react';
import {
	GlobalStyle,
  ThemeProvider
} from '@datapunt/asc-ui';
import Line from './components/Line/Line'
import styled from 'styled-components'

const dataSingle = [
  {
     "name": "winkels dagelijks",
     "value": 36
  },
  {
     "name": "supermarkt",
     "value": 3
  },
  {
     "name": "drogisterijen en apotheken",
     "value": 5
  },
  {
     "name": "winkels niet-dagelijks",
     "value": 98
  },
  {
     "name": "reparatie en verhuur",
     "value": 1
  },
  {
     "name": "horeca",
     "value": 54
  },
  {
     "name": "baliefuncties",
     "value": 9
  },
  {
     "name": "pers. verzorging",
     "value": 13
  },
  {
     "name": "medische diensten en kinderopvang",
     "value": 2
  }
]

const dataMulti = [
  {
    "name":"2009",
    "data": [
      { "name":"Amsterdam", "value": 5.8 },
      { "name":"G4", "value": 1.1 },
      { "name":"Nederland", "value": 2.3 }
    ]
  },
  { 
    "name":"2010",
    "data": [
      { "name":"Amsterdam", "value": 1.3 },
      { "name":"G4", "value": 1.7 },
      { "name":"Nederland", "value": 1.9 }
    ]
  },
  {
    "name":"2011",
    "data": [
      { "name":"Amsterdam", "value": 0 },
      { "name":"G4", "value": 2.3 },
      { "name":"Nederland", "value": 10 }
    ]
  }
];

const dataRapport = {
  title: [2016, 2018, 2020],
  data: [
    { name: 'Eerste optie', data: [5.3, 6.5, 8.1] },
    { name: 'Tweede optie', data: [1.3, 6.1, 7.2] },
    { name: 'Derde optie', data: [5.2, 7.1, 2.1] },
    { name: 'Vierde optie', data: [3.3, 5.5, 4.1] }
  ]
}

const colors = [
	'#ec0000',
	'#004699', 
	'#FF9100'
];

const dataLabel = (name) => (
	name === 'Amsterdam' ? 'in Amsterdam' :
	name === 'G4' ? 'in de G4' : 
	name === 'Nederland' ? 'in Nederland' : null
)

function App() {
	return (
		<ThemeProvider>
			<GlobalStyle />
      <div style={{ width: '500px' }}>
        <Line data={dataMulti} colors={colors} />
      </div>
		</ThemeProvider>
	);
}

export default App;