import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { registerHeightObserver } from 'element-height-observer'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

// setup app to work well in iframe
window.addEventListener('load', function () {
  // add class to body if document is in iframe to remove scrollbar in css
  if (window.top!==window.self) { document.body.className += " framed"}

  const root = document.getElementById('root')

  // send height to iframe onload
  let message = 'documentHeight:'+root.scrollHeight
  window.parent.postMessage(message,'*');

  // send height to iframe when height of root changes
  registerHeightObserver(root, { direction: 'vertical' }, function () {
    let message = 'documentHeight:'+root.scrollHeight
    window.parent.postMessage(message,'*');
  })
});
