import React from 'react';
import StackedBar from '../components/StackedBar/StackedBar'

export default {
  title: 'Stacked Bar Chart',
  component: StackedBar
};

const dataMulti = [
  {
    "name":"2009",
    "data": [
      { "name":"Amsterdam", "value": 0 },
      { "name":"G4", "value": 11 },
      { "name":"Nederland", "value": 23 },
    ]
  },
  { 
    "name":"2010",
    "data": [
      { "name":"Amsterdam", "value": 13 },
      { "name":"G4", "value": 17 },
      { "name":"Nederland", "value": 19 },
    ]
  },
  { 
    "name":"2011",
    "data": [
      { "name":"Amsterdam", "value": 58 },
      { "name":"G4", "value": 23 },
      { "name":"Nederland", "value": 10 },
    ]
  }
]

const colors = [
	'#ec0000',
	'#004699', 
	'#FF9100'
]

export const Stacked = () => 
<div style={{ width: '500px' }}>
  <StackedBar data={dataMulti} colors={colors} dataLabel={name => `% ${name}`} />
</div>

export const Loading = () => 
<div style={{ width: '500px' }}>
  <StackedBar isLoading />
</div>