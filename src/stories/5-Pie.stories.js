import React from 'react';
import Pie from '../components/Pie/Pie'

export default {
  title: 'Pie Chart',
  component: Pie
};

const dataSingle = [
  {
     "name": "winkels dagelijks",
     "value": 53
  },
  {
     "name": "supermarkt",
     "value": 20
  },
  {
     "name": "drogisterijen en apotheken",
     "value": 27
  }
]

const colors = [
	'#ec0000',
	'#004699', 
	'#FF9100'
]

export const SimplePie = () => 
<div style={{ width: '500px' }}>
  <Pie data={dataSingle} colors={colors} dataLabel={name => `% ${name}`} />
</div>

export const Loading = () => 
<div style={{ width: '500px' }}>
  <Pie isLoading />
</div>