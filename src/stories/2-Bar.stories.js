import React from 'react';
import Bar from '../components/Bar/Bar'

export default {
  title: 'Bar Chart',
  component: Bar
};

const dataSingle = [
  {
     "name": "winkels dagelijks",
     "value": 36
  },
  {
     "name": "supermarkt",
     "value": 3
  },
  {
     "name": "drogisterijen en apotheken",
     "value": 5
  },
  {
     "name": "winkels niet-dagelijks",
     "value": 98
  },
  {
     "name": "reparatie en verhuur",
     "value": 1
  },
  {
     "name": "horeca",
     "value": 54
  },
  {
     "name": "baliefuncties",
     "value": 9
  },
  {
     "name": "pers. verzorging",
     "value": 13
  },
  {
     "name": "medische diensten en kinderopvang",
     "value": 2
  }
]

export const SimpleBar = () => 
<div style={{ width: '500px' }}>
  <Bar data={dataSingle} color='#004699' />
</div>

export const Loading = () => 
<div style={{ width: '500px' }}>
  <Bar isLoading />
</div>