import React from 'react';
import Table from '../components/Table/Table'

export default {
  title: 'Table',
  component: Table
};

const dataRapport = {
  title: [2016, 2018, 2020],
  data: [
    { name: 'Eerste optie', data: [5.3, 6.5, 8.1] },
    { name: 'Tweede optie', data: [1.3, 6.1, 7.2] },
    { name: 'Derde optie', data: [5.2, 7.1, 2.1] },
    { name: 'Vierde optie', data: [3.3, 5.5, 4.1] }
  ]
}

export const DataTable = () => 
<div style={{ width: '500px' }}>
  <Table data={dataRapport} />
</div>