import React from 'react';
import ClusteredBar from '../components/ClusteredBar/ClusteredBar'

export default {
  title: 'Clustered Bar Chart',
  component: ClusteredBar
};

const dataMulti = [
  {
    "name":"2009",
    "data": [
      { "name":"Amsterdam", "value": 0 },
      { "name":"G4", "value": 11 },
      { "name":"Nederland", "value": 23 },
    ]
  },
  { 
    "name":"2010",
    "data": [
      { "name":"Amsterdam", "value": 13 },
      { "name":"G4", "value": 17 },
      { "name":"Nederland", "value": 19 },
    ]
  },
  { 
    "name":"2011",
    "data": [
      { "name":"Amsterdam", "value": 58 },
      { "name":"G4", "value": 23 },
      { "name":"Nederland", "value": 10 },
    ]
  }
]

const colors = [
	'#ec0000',
	'#004699', 
	'#FF9100'
]

export const Clustered = () => 
<div style={{ width: '500px' }}>
  <ClusteredBar data={dataMulti} colors={colors} dataLabel={name => `% ${name}`} />
</div>

export const Loading = () => 
<div style={{ width: '500px' }}>
  <ClusteredBar isLoading />
</div>
