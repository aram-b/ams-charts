import React from 'react';
import Line from '../components/Line/Line'

export default {
  title: 'Line Chart',
  component: Line
};

const dataSingle = [
  {
    "name":"2009",
    "data": [
      { "name":"Amsterdam", "value": 0 },
    ]
  },
  { 
    "name":"2010",
    "data": [
      { "name":"Amsterdam", "value": 1.3 },
    ]
  },
  {
    "name":"2011",
    "data": [
      { "name":"Amsterdam", "value": 5.8 },
    ]
  }
];

const dataMulti = [
  {
    "name":"2009",
    "data": [
      { "name":"Amsterdam", "value": 0 },
      { "name":"G4", "value": 11 },
      { "name":"Nederland", "value": 23 },
    ]
  },
  { 
    "name":"2010",
    "data": [
      { "name":"Amsterdam", "value": 13 },
      { "name":"G4", "value": 17 },
      { "name":"Nederland", "value": 19 },
    ]
  },
  { 
    "name":"2011",
    "data": [
      { "name":"Amsterdam", "value": 58 },
      { "name":"G4", "value": 23 },
      { "name":"Nederland", "value": 10 },
    ]
  }
]

const colors = [
	'#ec0000',
	'#004699', 
	'#FF9100'
]

export const SingleLine = () => 
<div style={{ width: '500px' }}>
  <Line data={dataSingle} colors={colors} dataLabel={name => `% ${name}`} />
</div>

export const MultiLine = () => 
<div style={{ width: '500px' }}>
  <Line data={dataMulti} colors={colors} dataLabel={name => `% ${name}`} />
</div>

export const Loading = () => 
<div style={{ width: '500px' }}>
  <Line isLoading />
</div>