import { GlobalStyle, ThemeProvider } from '@datapunt/asc-ui'
import { addDecorator } from '@storybook/react'
import React from 'react'
import styled from 'styled-components'
import '../node_modules/@datapunt/asc-assets/static/fonts/fonts.css';

const StoryWrapper = styled.div`
  position: relative;
  padding: 1rem;
`

function withGlobalStyles(storyFn) {
  return (
    <ThemeProvider>
      <>
        <GlobalStyle />
        <StoryWrapper>{storyFn()}</StoryWrapper>
      </>
    </ThemeProvider>
  )
}

addDecorator(withGlobalStyles)